require 'byebug'
class Code
  attr_reader :pegs
  PEGS = {
            "r" => "red",
            "g" => "green",
            "b" => "blue",
            "y" => "yellow",
            "o" => "orange",
            "p" => "purple"
          }

  def initialize(pegs_arr, size = 4)
    @pegs = parse_arr(pegs_arr, size)
    @size = size
  end

  def self.parse(string)
    Code.new(string.chars)
  end

  def self.random(size = 4)
    peg_string = String.new
    rand = Random.new
    keys = PEGS.keys
    size.times do
      peg_string << keys[rand.rand(keys.length)].to_s
    end
    self.parse(peg_string)
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(guess_code)
    exact_matches_hash = exact_matches_hash(guess_code)
    exact_matches_hash.values.reduce(0) { |sum, match| sum + match }
  end

  def near_matches(guess_code)
    exact_matches_hash = exact_matches_hash(guess_code)
    near_matches_hash = near_matches_hash(guess_code)
    near_matches = 0
    near_matches_hash.each do |key, value|
      value -= exact_matches_hash[key]
      near_matches += value
    end
    near_matches
  end

  def ==(other_code)
    return false unless other_code.is_a? Code
    pegs == other_code.pegs
  end

  def display
    @pegs.join
  end

  private

  def parse_arr(peg_arr, size)
    pegs = []
    (0...size).each do |idx|
      key = peg_arr[idx].downcase
      if PEGS[key]
        pegs << key
      else
        raise "Invalid color {char}"
      end
    end
    pegs
  end

  def exact_matches_hash(guess_code)
    hash = Hash.new(0)
    pegs.each_with_index do |val, index|
      hash[val] += 1 if val == guess_code[index]
    end
    hash
  end

  def near_matches_hash(guess_code)
    hash = Hash.new(0)
    code_string = pegs.join
    guess_code.pegs.each_with_index do |val|
      hash[val] += 1 if code_string.slice!(val)
    end
    hash
  end



end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random, tries = 4)
    @secret_code = code
    @tries = tries
  end

  def get_guess
    puts "Guess:"
    Code.parse($stdin.gets.chomp)
  end

  def display_matches(code)
    puts "exact matches : #{secret_code.exact_matches(code)}"
    puts "near matches : #{secret_code.near_matches(code)}"
  end

  def play
    until @tries == 0
      @tries -= 1
      guess_code = get_guess
      if guess_code == @secret_code
        puts "Congratulations you guess the secret code #{secret_code.display}"
        break
      else
        display_matches(guess_code)
        puts "Remaining tries: #{@tries}"
      end
    end
    puts "Sorry you lost, secret code was #{secret_code.display}" if @tries < 1
  end

end
